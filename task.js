// GENERAL:
const pokemonList = document.getElementById("pokemonList");
const chosenHero = document.getElementById("chosen");
const enemyHero = document.getElementById("enemy");
const closeGame = document.getElementById("closeGame");
const primaryAttack = document.getElementById("primaryAttack");
const mute = document.getElementById("mute");
const imgPlayer = document.getElementsByClassName("player");
const imgOpponent = document.getElementsByClassName("opponent");
const playAgain = document.getElementById("closeGame");
// AUDIO VARIABLES:
let isPlaying = false;
const song = new Audio("assets/battle.mp3");
const hitSound = new Audio("assets/hit.wav");

function hit() {
  hitSound.play();
}

function playSound() {
  isPlaying = true;
  if (isPlaying) {
    song.play();
  }
}

function stopSound() {
  isPlaying = false;
  if (!isPlaying) {
    song.pause();
    song.currentTime = 0;
  }
}

function muteSound() {
  isPlaying = false;
  if (!isPlaying) {
    song.pause();
  }
}
// CANVAS VARIABLES:
const canvas1 = document.getElementsByTagName("canvas")[0];
const canvas2 = document.getElementsByTagName("canvas")[1];
canvas1.width = 300;
canvas1.height = 300;
canvas1.style.width = "150px";
canvas1.style.height = "150px";

canvas2.width = 300;
canvas2.height = 300;
canvas2.style.width = "150px";
canvas2.style.height = "150px";

// BATTLE VARIABLES:
let pokemons = [];
let player = {};
let opponent = {};
let playerTurn = 0;

// FETCH ALL POKEMONS:
const fetchPokemon = () => {
  const promises = [];
  for (let i = 1; i <= 50; i++) {
    const url = `https://pokeapi.co/api/v2/pokemon/${i}`;
    promises.push(fetch(url).then((res) => res.json()));
  }
  Promise.all(promises).then((results) => {
    // console.log(data);
    pokemons = results.map((data) => new Pokemon(data));
    // console.log(pokemon);

    displayPokemon(pokemons);
  });
};

const displayPokemon = (pokemon) => {
  // console.log(pokemon)
  const pokemonHtmlString = pokemon.map(
    (pokemon) =>
      `
        <li>   
        <div class="card">
        <a data-toggle="modal" data-target="#exampleModal"><img class="card-img-top" src="${pokemon.spriteFront}" id="${pokemon.id}"></a>
        <h2 class="card-title">${pokemon.name}</h2>
        <p class="card-text">
        <span><b>Ability:</b> ${pokemon.ability.ability.name}</span> <br>
        <span><b>Move 1:</b> ${pokemon.move1}</span> <br>
        <span><b>Move 2:</b> ${pokemon.move2}</span> <br>
        <span><b>Move 3:</b> ${pokemon.move3}</span> <br>
        <span><b>Move 4:</b> ${pokemon.move4}</span> <br>
        <span><b>Speed:</b> ${pokemon.speed}</span> <br>
        <span><b>Special Defense:</b> ${pokemon.specialDefense}</span> <br>
        <span><b>Special Attack:</b> ${pokemon.specialAttack}</span> <br>
        <span><b>Defense:</b> ${pokemon.defense}</span> <br>
        <span><b>Attack:</b> ${pokemon.attack}</span> <br>
        <span><b>HP:</b> ${pokemon.currentHp}</span> <br>
        </p>
        </div>  
        </li>
        `
  );
  pokemonList.innerHTML = pokemonHtmlString;
};

fetchPokemon();

// SELECT POKEMON:
const selectPokemon = (event) => {
  const target = event.target.id;
  const found = pokemons.find((el) => el.id === +target);

  if (!found) {
    return;
  }
  playSound();
  player = Object.assign(new Pokemon(), found);
  console.log(player);
  opponent = generateOpponent(target);
  console.log(opponent);
  if (player.speed > opponent.speed) {
    console.log("1 First Player" + player.speed + " " + opponent.speed);
    playerTurn = 1;
    primaryAttack.removeAttribute("disabled");

    console.log(playerTurn);
  } else if (player.speed === opponent.speed) {
    playerTurn = Math.floor(Math.random() * 2 + 1);
    console.log(playerTurn);
    if (playerTurn === 1) {
      primaryAttack.removeAttribute("disabled");
    } else {
      normalAttack();
    }
  } else {
    console.log("2 Enemy Player" + player.speed + " " + opponent.speed);
    playerTurn = 2;
    primaryAttack.setAttribute("disabled", "disabled");

    normalAttack();
    console.log(playerTurn);
  }

  const pokemonHtmlString = `
    <span><b>${player.name}</b></span> <br>
    <span><b>HP: ${player.currentHp}</b></span> <br>
    <img class="player" src="${player.spriteBack}">
    `;
  chosenHero.innerHTML = pokemonHtmlString;
  console.log(imgPlayer);
};

// GENERATE OPPONENT:
const generateOpponent = (target) => {
  const opponent = Math.floor(Math.random() * pokemons.length);

  if (+opponent === +target) {
    return generateOpponent();
  } else {
    const found = pokemons.find((el) => el.id === +opponent);

    const pokemonHtmlString = `
    <span><b>${found.name}</b></span> <br>
    <span><b>HP: ${found.currentHp}</b></span> <br>
    <img class="opponent" src="${found.spriteFront}">
    `;

    enemyHero.innerHTML = pokemonHtmlString;
    console.log(imgOpponent);
    return Object.assign(new Pokemon(), found);
  }
};

// EVENT LISTENERS:
pokemonList.addEventListener("click", selectPokemon);
closeGame.addEventListener("click", stopSound);
mute.addEventListener("click", muteSound);
primaryAttack.addEventListener("click", normalAttack);
playAgain.addEventListener("click", reset);

function reset() {
   player = {};
   opponent = {};
   playerTurn = 0;
}

// ATTACKS
function normalAttack() {
  if (playerTurn === 1) {
    primaryAttack.setAttribute("disabled", "disabled");
    imgPlayer[0].classList.remove("attackEnemy");
    imgPlayer[0].classList.remove("blink");
    imgPlayer[0].classList.add("attackEnemy");
    setTimeout(() => {
      imgOpponent[0].classList.remove("blink");
      imgOpponent[0].classList.add("blink");
    }, 1000);
    player.executeAttack(opponent);
    setTimeout(() => {
      hit();
      const pokemonHtmlString = `
    <span><b>${opponent.name}</b></span> <br>
    <span><b>HP: ${opponent.currentHp}</b></span> <br>
    <img class="opponent" src="${opponent.spriteFront}">
    `;
      drawHealthbar2(
        200,
        200,
        (opponent.currentHp / opponent.baseHp) * 100,
        100,
        10
      );
      enemyHero.innerHTML = pokemonHtmlString;
      changeTurn();
      if (opponent.currentHp <= 0) {
        alert("You win!");
      } else {
      setTimeout(() => {
        normalAttack();
      }, 2000);
    }
    }, 900);
     
  } else {
    primaryAttack.removeAttribute("disabled");
    imgOpponent[0].classList.remove("attackHero");
    imgOpponent[0].classList.remove("blink");
    imgOpponent[0].classList.add("attackHero");
    setTimeout(() => {
      imgPlayer[0].classList.remove("blink");
      imgPlayer[0].classList.add("blink");
    }, 1000);
    opponent.executeAttack(player);
    setTimeout(() => {
      hit();
      const pokemonHtmlString = `
      <span><b>${player.name}</b></span> <br>
      <span><b>HP: ${player.currentHp}</b></span> <br>
      <img class="player" src="${player.spriteBack}">
      `;
      chosenHero.innerHTML = pokemonHtmlString;
      drawHealthbar1(
        200,
        200,
        (player.currentHp / player.baseHp) * 100,
        100,
        10
      );
      changeTurn();
      if (player.currentHp <= 0) {
        alert("You lose!");
      }
    }, 900);
    
  }
}

// CANVAS HEALTHBARS:
const chosenHeroHP = document.getElementById("chosenHero");
const ctx1 = chosenHeroHP.getContext("2d");

const enemyHeroHP = document.getElementById("enemyHero");
const ctx2 = enemyHeroHP.getContext("2d");

const drawHealthbar1 = (x, y, per, width, thickness) => {
  ctx1.beginPath();
  ctx1.rect(x - width / 2, y, width * (per / 100), thickness);
  if (per > 50) {
    ctx1.fillStyle = "#03fc30";
    ctx1.border = "3px solid black";
  } else if (per > 10) {
    ctx1.fillStyle = "gold";
  } else {
    ctx1.fillStyle = "red";
  }
  ctx1.closePath();
  ctx1.fill();
};

drawHealthbar1(200, 200, 100, 100, 10);

const drawHealthbar2 = (x, y, per, width, thickness) => {
  ctx2.beginPath();
  ctx2.rect(x - width / 2, y, width * (per / 100), thickness);
  if (per > 50) {
    ctx2.fillStyle = "#03fc30";
  } else if (per > 10) {
    ctx2.fillStyle = "gold";
  } else {
    ctx2.fillStyle = "red";
  }
  ctx2.closePath();
  ctx2.fill();
};

drawHealthbar2(200, 200, 100, 100, 10);

// BATTLE SIMULATION:

class Pokemon {
  constructor(data) {
    if (data) {
      this.id = data.id;
      this.name = data.name;
      this.ability = data.abilities.find(
        (ability) => ability.is_hidden === false
      );
      this.move1 = data.moves[0].move.name;
      this.move2 = data.moves[1].move.name;
      this.move3 = data.moves[2].move.name;
      this.move4 = data.moves[3].move.name;

      this.baseHp = data.stats[0].base_stat;
      this.currentHp = data.stats[0].base_stat;
      this.attack = data.stats[1].base_stat;
      this.defense = data.stats[2].base_stat;
      this.specialAttack = data.stats[3].base_stat;
      this.specialDefense = data.stats[4].base_stat;
      this.speed = data.stats[5].base_stat;

      this.spriteFront = data.sprites.front_default;
      this.spriteBack = data.sprites.back_default;
    }
  }
  executeAttack(enemy) {
    enemy.takeDamage(this.attack);
  }
  takeDamage(dmg) {
    console.log(dmg);
    const inflictedDmg = Math.floor((dmg / this.defense) * Math.random() * 200);
    this.currentHp -= inflictedDmg;
    console.log(this.currentHp + " currentHP");
    console.log(inflictedDmg + " inflicted!");
    if (this.currentHp < 0) {
    this.currentHp = 0;
    }
  }
}

// ATTACK FORMULA:
//(Attack / Opponent Defense) * A random number between 0 and 200

// 50/50 chance function
// Math.floor(Math.random() * 2 + 1)

// CHANGE TURN FUNCTION:
function changeTurn() {
  if (playerTurn === 1) {
    playerTurn = 2;
  } else {
    playerTurn = 1;
  }
}
